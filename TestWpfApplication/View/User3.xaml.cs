﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TestWpfApplication.ViewModel;

namespace TestWpfApplication.View
{
    /// <summary>
    /// Interaction logic for User3.xaml
    /// </summary>
    public partial class User3 : Window
    {
        private User3ViewModel _user3ViewModel = new User3ViewModel();
        public User3()
        {
            InitializeComponent();
            DataContext = _user3ViewModel;
        }
    }
}
