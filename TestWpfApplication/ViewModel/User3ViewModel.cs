﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Data;
using TestWpfApplication.Annotations;

namespace TestWpfApplication.ViewModel
{
    class User3ViewModel : INotifyPropertyChanged
    {

        private WpfAppEntities dataContext;
        public BackgroundWorker worker1;
        public BackgroundWorker worker2;

        public string Text1 { get; set; }
        public string Text2 { get; set; }

        public CustomCommand startStop { get; set; }
        public List<UserMessage> UserMessages { get; set; }

        public bool IsStarted = false;

        public User3ViewModel()
        {
            dataContext = new WpfAppEntities();
            UserMessages = new List<UserMessage>(dataContext.UserMessages);
            startStop = new CustomCommand(StartWorkers) {IsExecutable = true};
            worker1 = new BackgroundWorker();
            worker2 = new BackgroundWorker();

            worker1.WorkerSupportsCancellation = true;
            worker2.WorkerSupportsCancellation = true;

            worker1.DoWork += worker1_DoWork;
            worker2.DoWork += worker2_DoWork;

            worker1.RunWorkerCompleted += worker1_RunWorkerCompleted;
            worker2.RunWorkerCompleted += worker2_RunWorkerCompleted;


        }

        public void StartWorkers()
        {
            if (!IsStarted)
            {
                worker1.RunWorkerAsync();
                worker2.RunWorkerAsync();
                IsStarted = true;
            }
            else
            {
                
                worker1.CancelAsync();
                worker2.CancelAsync();
                IsStarted = false;
            }

            
        }

        public void worker1_DoWork(object sender, DoWorkEventArgs e)
        {
            foreach (var user in UserMessages)
            {
                if (worker1.CancellationPending == true)
                {
                    e.Cancel = true;
                    return;
                }

                Text1 = user.Message;
                Thread.Sleep(1000);
                RaisePropertyChanged("Text1");
            }
        }

        public void worker2_DoWork(object sender, DoWorkEventArgs e)
        {
            foreach (var user in UserMessages)
            {
                if (worker2.CancellationPending == true)
                {
                    e.Cancel = true;
                    return;
                }

                Text2 = user.Message;
                Thread.Sleep(2000);
                RaisePropertyChanged("Text2");
            }
        }

        public void worker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                Text1 = "Cancelled by user";
                RaisePropertyChanged("Text1");
            }
            else
            {
                
                Text1 = "Done";
                RaisePropertyChanged("Text1");
            }

        }

        public void worker2_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                Text2 = "Cancelled by user";
                RaisePropertyChanged("Text2");
            }
            else
            {

                Text2 = "Done";
                RaisePropertyChanged("Text2");
            }

        }


        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged == null)
            {
                return;
            }
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
