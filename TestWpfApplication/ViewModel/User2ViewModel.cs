﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Data;

namespace TestWpfApplication.ViewModel
{
    class User2ViewModel : INotifyPropertyChanged
    {

        private WpfAppEntities _dataContext;
        public List<UserMessage> UserMessages { get; set; }
        public CustomCommand LoadMessages { get; set; }
        public string Login { get; set; }

        public User2ViewModel()
        {
            _dataContext = new WpfAppEntities();
            LoadMessages = new CustomCommand(Load) {IsExecutable = true};
        }

        public void Load()
        {
            UserMessages = new List<UserMessage>(_dataContext.UserMessages);
            RaisePropertyChanged("UserMessages");
        }


        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Метод для вызова события об изменении свойства ViewModel.
        /// </summary>
        /// <param name="propertyName"></param>
        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged == null)
            {
                return;
            }
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        
    }
}
