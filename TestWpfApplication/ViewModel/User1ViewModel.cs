﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using TestWpfApplication.View;

namespace TestWpfApplication.ViewModel
{
    class User1ViewModel
    {
        public string ColorText { get; set; }

        public CustomCommand OpenWindow { get; set; }

        public User1ViewModel()
        {
            OpenWindow = new CustomCommand(Window) {IsExecutable = true};
        }


        public void Window()
        {

            if (ColorText != null)
            {
                ColoredMessageBox _coloredMessageBox = new ColoredMessageBox();
                Color color = new Color();
                var convertFromString = ColorConverter.ConvertFromString(ColorText);
                if (convertFromString != null)
                {
                    color = (Color)convertFromString;
                }
                _coloredMessageBox.Background = new SolidColorBrush(color);
                _coloredMessageBox.Show();
            }
            else
            {
                MessageBox.Show("Pick the color");
            }

            
        }
        

        
    }
}
