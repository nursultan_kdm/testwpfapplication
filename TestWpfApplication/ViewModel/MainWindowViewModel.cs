﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Data;
using TestWpfApplication.View;

namespace TestWpfApplication.ViewModel
{
    class MainWindowViewModel
    {
        private WpfAppEntities _dataContext;

        public string Login { get;set; }
        public string Password { get; set; }

        public CustomCommand LoginCommand { get; set; }

        public MainWindowViewModel()
        {
            _dataContext = new WpfAppEntities();
            LoginCommand = new CustomCommand(LogIn) {IsExecutable = true};
        }

        public void LogIn()
        {

            var user = _dataContext.Users.Where(x => x.UserLogin == Login).FirstOrDefault();


            if (user == null)
            {
                MessageBox.Show("Wrong login or password.");
            }
            else if (Login == user.UserLogin && Password == user.UserPassword)
            {
                MessageBox.Show(user.UserLogin + ", you have successfully logged in.");
                if (user.UserLogin == "user1")
                {
                    User1 window = new User1();
                    window.Show();
                }
                else if (user.UserLogin == "user2")
                {
                    User2 window = new User2();
                    window.Show();
                }
                else if(user.UserLogin == "user3")
                {
                    User3 window = new User3();
                    window.Show();
                }
            }
            else
            {
                MessageBox.Show("Wrong login or password.");
            }

        }

        

    }
}
